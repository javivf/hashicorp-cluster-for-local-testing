"Nomad + Consul for local testing" uses vagrant and ansible to set up a cluster fastly

Strongly "inspired" on https://github.com/discoposse/nomad-vagrant-lab and https://gitlab.com/javivf/kubernetes-for-local-testing

Run `vagrant up` and profit!

* Check the services are running properly in the nodes
```
sudo supervisorctl status
```

* Run this command in node-2 and node-3 to join node-1 and have a node leader
```
sudo nomad server join 192.168.51.2
```

* To open the Nomad UI use this command
```
open http://192.168.51.2:4646
```

* To open the Nomad UI use this command
```
open http://192.168.51.2:8500
```
